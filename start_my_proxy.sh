cp 01proxy '/etc/apt/apt.conf.d/' && echo 'Used proxy in 01proxy. APT should work now.'
echo 'Try following to get little more access(wget and curl):'
echo 'export http_proxy="http://172.16.19.11:80"'
echo 'Use following for Netcat(nc):'
echo 'nc -X connect -x <proxy-address>:<port> <url-address> <port>'
echo 'Use following for pip3:'
echo 'pip3 install --proxy http://<proxy-server>:<port> <package-name>'
#echo 'http_proxy="http://172.16.19.11:80"' | cat >> 'a.txt';
#grep -q -F 'http_proxy="http://172.16.19.11:80"' '/etc/environment' || echo 'http_proxy="http://172.16.19.11:80"' >> '/etc/environment';
#export http_proxy="http://172.16.19.11:80"